<?php

namespace Abetzi\Elasticsearch\Commands;

use Abetzi\Elasticsearch\Services\Alias;
use Abetzi\Elasticsearch\Services\Index;
use Illuminate\Console\Command;

class Info extends Command
{
    protected $signature = 'es:index:info
        {pattern : Index name}';

    protected $description = 'Show info on given index and its related aliases';

    public function __construct(
        private Alias $aliasService,
        private Index $indexService,
    ) {
        parent::__construct();
    }

    public function handle()
    {
        $pattern = $this->argument('pattern');

        // aliasy s patternem
        $aliases = $this->aliasService->list($pattern);

        // indedexu s patternem
        $indices = $this->indexService->list($pattern);
        $this->line('');
        $this->info(sprintf('Indices with name pattern "%s"', $pattern));
        $this->line('');
        foreach($indices as $index) {
            $stars = in_array($index, $aliases)? ' * %s': '   %s';
            $this->line(sprintf($stars, $index));
        }
        $this->line('');

        return Command::SUCCESS;
    }
}
