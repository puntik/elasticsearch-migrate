<?php

namespace Abetzi\Elasticsearch\Commands;

use Abetzi\Elasticsearch\Services\Index;
use Abetzi\Elasticsearch\Services\Template;
use Illuminate\Console\Command;

class CreateTemplate extends Command
{

    protected $signature = 'es:template:create
        {template : Template name}';

    protected $description = 'Create elasticsearch template from given yaml file';

    private Index $createService;

    public function __construct(
        public Template $templateService
    ) {
        parent::__construct();
    }

    public function handle(): int
    {
        $indexName = $this->argument('template');

        try {
            $response = $this->templateService->create($indexName);

            $this->newLine();
            $this->info(sprintf('Template "%s" was successfully created.', $indexName));
            $this->newLine();

        } catch (\Throwable $exception) {
            $this->error($exception->getMessage());
        }

        return self::SUCCESS;
    }
}
