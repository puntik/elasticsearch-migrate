<?php

declare(strict_types=1);

namespace Abetzi\Elasticsearch;

use Abetzi\Elasticsearch\Services\Index;
use Elastic\Elasticsearch\Client;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Builder;
use Laravel\Scout\Engines\Engine;

class Driver extends Engine
{

    public function __construct(
        private Client $indexClient,
        private Index $indexService
    ) {
    }

    /**
     * update
     *
     * @param  Collection $models
     * @return void
     */
    public function update($models): void
    {
        if (empty($models)) {
            return;
        }

        $params = [];

        foreach ($models as $model) {
            $params['body'][] = [
                'index' => [
                    '_index' => $model->searchableAs(),
                ],
            ];

            $params['body'][] = $model->toSearchableArray();
        }

        $this->indexClient->bulk($params);
    }

    public function flush($model): void
    {
        $this->indexClient->deleteByQuery([
            'index' => $model->searchableAs(),
            'body'  => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);
    }

    public function delete($models): void
    {
        $models->each(function ($model) {
            $params = [
                'index' => $model->searchableAs(),
                'id'    => $model->getScoutKey(),
            ];

            $this->indexClient->delete($params);
        });
    }

    /**
     * @param Builder $builder
     * @param int     $perPage
     * @param int     $page
     *
     * @return array|callable|mixed
     */
    public function paginate(Builder $builder, $perPage, $page)
    {
        $body = [
            'size'  => $perPage,
            'from'  => $page,
            'query' => [
                'multi_match' => [
                    'fields' => array_keys($builder->model->toSearchableArray()),
                    'query'  => $builder->query,
                ],
            ],
        ];

        $params = [
            'index' => $builder->index,
            'body'  => $body,
        ];

        return $this->doSearch($params);
    }

    public function search(Builder $builder)
    {
        return $this->paginate($builder, 20, 1);
    }

    private function doSearch(array $params)
    {
        return $this->indexClient->search($params);
    }

    public function mapIds($results): \Illuminate\Support\Collection
    {
        return collect($results['hits']['hits'])->pluck('_id');
    }

    /**
     * @param Builder $builder
     * @param mixed   $results
     * @param Model   $model
     *
     * @return Collection|\Illuminate\Support\Collection
     * @throws BindingResolutionException
     */
    public function map(Builder $builder, $results, $model)
    {
        return $builder->model->getScoutModelsByIds(
            $builder,
            $this->mapIds($results)->toArray()
        );
    }

    public function getTotalCount($results): int
    {
        return $results['hits']['total']['value'];
    }

    public function lazyMap(Builder $builder, $results, $model)
    {
        // TODO: Implement lazyMap() method.
    }

    public function createIndex($name, array $options = [])
    {
        $index = $this->indexService->create($name);
    }

    public function deleteIndex($name)
    {
        $this->indexService->delete($name);
    }
}
