<?php

namespace Abetzi\Elasticsearch\Helpers;

use Elastic\Elasticsearch\Client;
use Illuminate\Support\Facades\App;
use Nette\Utils\Json;

class Helper
{
    public static function info(): array
    {
        $info = static::clientInfo();
        $connection = static::connection();

        return array_merge($info, $connection);
    }

    /**
     * @todo neni definovany client
     * @todo klient neni pripojen k zivemu clusteru
     *
     * @return array
     */
    private static function connection(): array
    {
        /** @var Client */
        $elasticsearch = App::get(Client::class);
        $info = [];

        try {
            $response = $elasticsearch->info([])->asArray();

            return [
                'Cluster version' => $response['version']['number'],
                'Tagline' => $response['tagline'],
            ];
        }
        catch(\Throwable $e) {
            $info = [
                'Cluster version' => '<fg=red>No alive node.</>',
            ];
        }

        return $info;
    }

    /**
     * @todo vsechno ok
     * @todo neexistuje composer lock
     * @todo parsovani composer.lock selze
     * @todo neexistuje knihovna v composer.lock
     *
     * @return array
     */
    private static function clientInfo(): array
    {
        $path = app_path('../composer.lock');
        $json = file_get_contents($path);
        $dataDecoded = Json::decode($json, true);

        $package = collect($dataDecoded['packages'])
            ->filter(function($item) {
                return $item['name'] === 'elasticsearch/elasticsearch';
            })
            ->first();

        return [
            'Client version' => $package['version'],
        ];
    }
}