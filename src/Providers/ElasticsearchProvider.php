<?php

namespace Abetzi\Elasticsearch\Providers;

use Abetzi\Elasticsearch\Commands\CreateTemplate;
use Abetzi\Elasticsearch\Commands\DeleteIndex;
use Abetzi\Elasticsearch\Commands\Info;
use Abetzi\Elasticsearch\Driver;
use Abetzi\Elasticsearch\Helpers\Helper;
use Abetzi\Elasticsearch\Services\Index;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Foundation\Console\AboutCommand;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;

class ElasticsearchProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function (): Client {
            return ClientBuilder::create()
                ->setBasicAuthentication(config('services.elasticsearch.username'), config('services.elasticsearch.password'))
                ->setSSLVerification(config('services.elasticsearch.ssl_verification'))
                ->setHosts(config('services.elasticsearch.hosts'))
                ->build();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        AboutCommand::add('Elasticsearch', Helper::info());

        resolve(EngineManager::class)->extend('elasticsearch', function () {
            return new Driver(
                App::get(Client::class),
                App::make(Index::class));
        });

        if($this->app->runningInConsole()) {
            $this->commands([
                CreateTemplate::class,
                Info::class,
            ]);
        }
    }
}
