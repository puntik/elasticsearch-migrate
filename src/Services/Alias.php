<?php

namespace Abetzi\Elasticsearch\Services;

use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Http\Client\Common\Exception\ClientErrorException;
use Http\Client\Common\Exception\ServerErrorException;

class Alias
{
    public function __construct(
        private Client $elasticsearch
    ) {}

    /**
     * It lists all indices conneced to alias. Empty,
     *
     * @param string $aliasName
     * @return array
     */
    public function list(string $aliasName): array
    {
        try {
            // get all indexes with alias
            $response = $this->elasticsearch->indices()->getAlias([
                'name' => $aliasName,
            ]);
        } catch(ClientResponseException | ServerErrorException $e) {
            return [];
        }

        return collect(array_keys($response->asArray()))->sort()->toArray();
    }

    public function clean(string $alias, array $indices): bool
    {
        if (empty($indices)) {
            return false;
        }

        // delete old aliases
        $response  = $this->elasticsearch->indices()->deleteAlias([
            'index' => $indices,
            'name' => $alias,
        ]);

        return $response['acknowledged'];
    }
}
