<?php

namespace Abetzi\Elasticsearch\Services;

use Elastic\Elasticsearch\Client;
use Symfony\Component\Yaml\Yaml;

class Template {

    private const CONFIG_DIR = 'curator';

    public function __construct(
        private Client $elasticsearch
    ) {}

    /*
     * test: neexistuje soubor
     * test: neda se parsovat yaml
     * test: template neni obaleny elementem template
     */
    public function create(string $name): bool
    {
        $filename = database_path(sprintf('%s/%s-template.yml', self::CONFIG_DIR, $name));

        throw_unless(
          is_file($filename),
          new \InvalidArgumentException('There is no such file')
        );

        $template = Yaml::parseFile($filename);

        $params = [
            'name' => sprintf('%s_template', $name),
            'body' => $template['template'],
        ];

        $response = $this->elasticsearch->indices()->putTemplate($params);

        return $response['acknowledged'];
    }
}
