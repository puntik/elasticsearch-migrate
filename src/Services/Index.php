<?php declare(strict_types=1);

namespace Abetzi\Elasticsearch\Services;

use Elastic\Elasticsearch\Client;

class Index
{

    public function __construct(
        private Alias $aliasService,
        private Client $elasticsearch,
    ){}

    public function create(string $name): string
    {
        $realIndexName = $this->createNewIndex($name);

        return $realIndexName;
    }

    public function delete(string $name): void
    {
        // 1 .. najdi alias s pattername $name
        // 2 .. najdi prislusny index
        // 3 .. smaz alias a index

        throw new \Exception('Not implemented yet');
    }

    public function prune(string $pattern): array
    {
        // smazat vsechny indexy daneho patteru

        // 1 .. najdi vsechny indexy s danym patternem
        $indices = $this->list($pattern);

        // 2 .. najdi pattern jako alias a vyrad ze seznamu
        $alliased = $this->aliasService->list($pattern);

        // 3 .. vsechno smaz
        return array_map(function($index) {
            $this->elasticsearch->indices()->delete([
                'index' => $index
            ]);

            return $index;
        }, array_diff($indices, $alliased));
    }

    public function list(string $indexName): array
    {
        // find a new index order number
        $resp = $this->elasticsearch->indices()->get([
            'index' => sprintf('%s-*', $indexName),
        ]);

        return array_keys($resp->asArray());
    }

    private function createNewIndex(string $indexName): string
    {
        // get all old aliased indices
        $oldAliasedIndices = $this->aliasService->list($indexName);

        // new ordered index name
        $orderedIndexName = $this->createOrderedIndexName($indexName);

        // create new index
        $params = [
            'index' => $orderedIndexName,
            'body' => [
                'aliases' => [
                    $indexName => new \stdClass(),
                ],
            ],
        ];

        $this->elasticsearch->indices()->create($params);

        // clean old aliases
        $this->aliasService->clean($indexName, $oldAliasedIndices);

        return $orderedIndexName;
    }

    private function createOrderedIndexName(string $indexName): string
    {
        $indices = $this->list($indexName);

        $last = collect($indices)->sort()->last();

        if ($last === null) {
            $newOrder = 1;
        } else {
            preg_match(sprintf('/^%s-(\d{6})$/', $indexName), $last, $matches);
            $newOrder = ((int)$matches[1]) + 1;
        }

        return sprintf('%s-%06d', $indexName, $newOrder);
    }
}
