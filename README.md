# Elasticsearch migrate

Simple elasticsearch indices manipulation for laravel framework.

## Basic Usage

~~~bash
artisan es:index:info              Show info on given index and its related aliases
artisan es:template:create         Create elasticsearch template from given yaml file
~~~

~~~bash
artisan scout:delete-all-indexes   Delete all indexes
artisan scout:delete-index         Delete an index
artisan scout:flush                Flush all of the models records from the index
artisan scout:import               Import the given model into the search index
artisan scout:index                Create an index
~~~

~~~bash
artisan about                       Info about elasticsearch connection
~~~

~~~shell
SCOUT_DRIVER=elasticsearch

ELASTICSEARCH_HOST=localhost:9200
ELASTICSEARCH_USERNAME=elastic
ELASTICSEARCH_PASSWORD=elastic
ELASTICSEARCH_SSL_VERIFICATION=false
~~~

## Install - as git submodule for development purpose

First - download source files
~~~bash
git submodule add git@bitbucket.org:puntik/elasticsearch-migrate.git
~~~

Install required libraries
~~~bash
composer install elasticsearch/elasticsearch
composer install laravel/scout
~~~
Then inject provider into config/app.php

~~~php
<?php
[
  'providers' => [
      // ...
      /*
       * Package Service Providers...
       */
      Abetzi\Elasticsearch\Providers\ElasticsearchProvider::class,
    ],
]
~~~

Do not forget add autoload psr-4 mapping into composer.json.

~~~json
{
    "autoload": {
        "psr-4": {
            "Abetzi\\Elasticsearch\\": "src/elasticsearch-migrate/src/"
        }
    }
}
~~~

Configure config/services.php
~~~php
<?php
return [
    // ... 
    'elasticsearch' => [
        'hosts'    => explode(',', env('ELASTICSEARCH_HOST', 'https://host.docker.internal:9210')),
        'username' => env('ELASTICSEARCH_USERNAME', 'elastic'),
        'password' => env('ELASTICSEARCH_PASSWORD', 'elastic'),
        'ssl_verification' => env('ELASTICSEARCH_SSL_VERIFICATION', true)
    ],
];
~~~

## Install - composer

Not finished yet.

- composer install ...
- config .env
- artisan publish ... 
- it should work

## Use case - index management
- [x] Chci zalozit novy index s poradovym cislem podle vzoru {alias_name}-000001, alias neexistuje
- [x] Chci zalozit novy index s poradovym cislem podle vzoru {alias_name}-000001, odebrat stare aliasy a nastavit novy alias.
- [x] Chci nacist yaml s template a ulozit do indexu
- [ ] Chci smazat index, na ktery je pripojeny alias {alias_name}.
- [ ] Chci smazat vsechny indexy s patternem {alias_name}-000001, na ktere nesmeruje alias {alias_name}
- [ ] Chci vypsat vsechny informace o indexech a aliasech s patternem {name}

## Todo:

- [x] refaktorovat
- [ ] parametrizovat
- [x] rozepsat do service
- [x] udelat providery
- [x] udelat commandy
- [ ] napsat navod
- [ ] nasypat do packagest - pripravit composer.json
- [ ] sepsat povidani na tema konfigurace klienta elasticsearch
- [ ] scout:create-index - implemetnovat misto moji tridy
- [ ] scout:delete-index - implementovat misto moji tridy 
