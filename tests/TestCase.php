<?php
namespace Abetzi\Elasticsearch\Test;

use Elastic\Elasticsearch\Client as ElasticsearchClient;
use Elastic\Elasticsearch\ClientBuilder;
use Elastic\Elasticsearch\Response\Elasticsearch;
use GuzzleHttp\Psr7\Response;
use Http\Mock\Client as HttpClient;

class TestCase extends \PHPUnit\Framework\TestCase
{
    public function createClient(array $elasticsearchResponse, int $status = 200): ElasticsearchClient
    {
        $httpClient = $this->createHttpClient($elasticsearchResponse, $status);

        $builder = ClientBuilder::create();
        $builder->setHosts(['http://localhost']);
        $builder->setHttpClient($httpClient);

        return $builder->build();
    }

    private function createHttpClient(array $response, int $status = 200): HttpClient
    {
        $httpClient = new HttpClient();

        // This is a PSR-7 response
        $httpResponse = new Response(
            $status,
            [
                Elasticsearch::HEADER_CHECK => Elasticsearch::PRODUCT_NAME,
                'Content-Type' => 'application/json'
            ],
            json_encode($response)
        );

        $httpClient->addResponse($httpResponse);

        return $httpClient;
    }
}
