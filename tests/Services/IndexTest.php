<?php

class IndexTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function it_lists_all_indices_by_name()
    {
        $this->markTestIncomplete();
    }

    /**
     * @test
     */
    public function it_creates_a_new_index_with_alias()
    {
        $this->markTestIncomplete();
    }

    /**
     * @test
     */
    public function it_deletes_an_index_and_its_alias()
    {
        $this->markTestIncomplete();
    }

    /**
     * @test
     */
    public function it_prunes_not_aliased_indices()
    {
        $this->markTestIncomplete();
    }
}
