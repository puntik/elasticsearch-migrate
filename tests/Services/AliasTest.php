<?php

namespace Abetzi\Elasticsearch\Test\Service;

use Abetzi\Elasticsearch\Services\Alias;
use Abetzi\Elasticsearch\Test\TestCase;
use Elastic\Elasticsearch\Exception\ClientResponseException;

class AliasTest extends TestCase
{
    /**
     * @test
     */
    public function it_lists_all_aliases()
    {
        //Given
        $elasticsearchClient = $this->createClient([
            "books-000027" => [
                "aliases" => [
                    "books" => []
                ]
            ]
        ]);

        $aliasService = new Alias($elasticsearchClient);

        // When
        $aliases = $aliasService->list('books');

        // Then
        $this->assertCount(1, $aliases);
        $this->assertContains('books-000027', $aliases);
    }

    /**
     * @test
     */
    public function it_fails_when_alias_does_not_exist()
    {
        // Given
        $elasticsearchClient = $this->createClient([
            "error" => "alias [book] missing",
            "status" => 404
        ],
            404
        );

        $aliasService = new Alias($elasticsearchClient);

        // Expected
        $this->expectException(
            ClientResponseException::class
        );

        // When
        $aliasService->list('book');
    }

    /**
     * @test
     */
    public function it_cleans_aliases()
    {
        $elasticsearchClient = $this->createClient([
            'acknowledged' => true
        ]);

        $aliasService = new Alias($elasticsearchClient);

        $response = $aliasService->clean('books', ['books-000001']);

        $this->markTestIncomplete();
    }
}
