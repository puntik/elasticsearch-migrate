<?php

namespace Abetzi\Elasticsearch\Test\Service;

use Abetzi\Elasticsearch\Test\TestCase;

class TemplateTest extends TestCase
{
    /**
     * @test
     */
    public function it_loads_yaml_file_and_creates_a_new_elasticsearch_template()
    {
        $this->markTestIncomplete();
    }

    /**
     * @test
     */
    public function it_fails_when_yaml_file_does_not_exist()
    {
        $this->markTestIncomplete();
    }
}
